package tipoEnemigo;

import java.awt.Graphics2D;

import hilera.Hilera;
import pantalla.Jugador;

/**
 * 
 * @author mende_000
 *
 */
public interface TipoEnemigo {
	/**
	 * Dibuja
	 * 
	 * @param g
	 */
	void draw(Graphics2D g);

	/**
	 * Actualiza
	 * 
	 * @param delta
	 * @param jugador
	 */
	void update(double delta, Jugador jugador);

	/**
	 * Cambia la direccion
	 * 
	 * @param delta
	 */
	void cambiarDireccion(double delta);

	/**
	 * Verifica si hay muerte
	 * 
	 * @return: True si hay muerte, false si no hay muerte
	 */
	boolean muerte();

	/**
	 * Valida las coliciones
	 * 
	 * @param i:
	 *            posicion del enemigo
	 * @param jugador:
	 *            nave jugador
	 * @param lista:
	 *            hilera de enemigos
	 * @return: true si hay colicion, false si no hay colicion
	 */
	boolean colision(int i, Jugador jugador, Hilera lista);

	/**
	 * Valida los limites
	 * 
	 * @return: true si hay esta fuera de los limites, false si no esta fuera de los
	 *          limites
	 */
	boolean fueraRango();
}
