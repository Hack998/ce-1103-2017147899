package tipoEnemigo;

import java.awt.Graphics2D;
import java.awt.Rectangle;
import display.Display;
import hilera.Hilera;
import pantalla.Jugador;
import pantalla.Pantalla;
import sprite.Sprite;

/**
 * 
 * @author mende_000
 *
 */
public class EnemigoJefe implements TipoEnemigo {

	private double speed = 3.0d * Pantalla.getNivel();
	private int vida = (int) (Math.random() * 5) + 2;
	private Rectangle rect;
	private Sprite enemigoSprite;

	/**
	 * Constructor
	 * 
	 * @param xPos:
	 *            posicion en x
	 * @param yPos:
	 *            posicion en y
	 * @param width:
	 *            ancho
	 * @param height:
	 *            alto
	 */
	public EnemigoJefe(double xPos, double yPos, int width, int height) {
		setEnemigoSprite(new Sprite(xPos, yPos, width, height, "/imagenes/jefe.png"));
		getEnemigoSprite().setWidth(25);
		getEnemigoSprite().setHeight(25);
		this.setRect(new Rectangle((int) getEnemigoSprite().getxPos(), (int) getEnemigoSprite().getyPos(),
				getEnemigoSprite().getWidth(), getEnemigoSprite().getHeight()));
	}

	/**
	 * Dibuja
	 */
	@Override
	public void draw(Graphics2D g) {
		getEnemigoSprite().draw(g);
	}

	/**
	 * Actualiza
	 */
	@Override
	public void update(double delta, Jugador jugador) {
		getEnemigoSprite().update(delta);
		getEnemigoSprite().setxPos(getEnemigoSprite().getxPos() - (delta * speed));
		this.getRect().x = (int) getEnemigoSprite().getxPos();
	}

	/**
	 * Cambia la direccion
	 */
	@Override
	public void cambiarDireccion(double delta) {
		speed *= -1.05d;
		getEnemigoSprite().setxPos(getEnemigoSprite().getxPos() - (delta * speed));
		this.getRect().x = (int) getEnemigoSprite().getxPos();

		getEnemigoSprite().setyPos(getEnemigoSprite().getyPos() + (delta * 40));
		this.getRect().y = (int) getEnemigoSprite().getyPos();
	}

	/**
	 * Valida si hay muerte
	 */
	@Override
	public boolean muerte() {
		if (!getEnemigoSprite().isPlay())
			return false;
		if (getEnemigoSprite().AnimationDestroy())
			return true;
		return false;
	}

	/**
	 * Valida las coliciones
	 */
	@Override
	public boolean colision(int i, Jugador jugador, Hilera enemigos) {

		if (getEnemigoSprite().isPlay()) {
			if (((EnemigoJefe) enemigos.getValor(i)).muerte()) {
				enemigos.removerPorPosicion(i);
			}
			return false;
		}
		for (int w = 0; w < jugador.weapon.weapons.size(); w++) {
			if (enemigos != null
					&& jugador.weapon.weapons.get(w).colicionRect(((EnemigoJefe) enemigos.getValor(i)).getRect())) {
				getEnemigoSprite().Player(true, true);
				Pantalla.setScore(Pantalla.getScore() + 20);
				enemigos.removerPorPosicion(i);
				Pantalla.setMuertes(Pantalla.getMuertes() + 1);
				Pantalla.setContador(Pantalla.getContador() + 1);
				if (Pantalla.getHilera() == 2 || Pantalla.getHilera() == 3) {
					Pantalla.setContador(5);
					Pantalla.setScore(Pantalla.getScore() + ((enemigos.getTama�o() - 1) * 10));
					Pantalla.setMuertes(Pantalla.getMuertes() + (enemigos.getTama�o() - 1));
					enemigos.eliminar();
				}
				return true;
			}
		}
		return false;
	}

	/**
	 * Valida los limites
	 */
	@Override
	public boolean fueraRango() {
		if (rect.x > 0 && rect.x < Display.WIDTH - rect.width)
			return false;
		return true;
	}

	/**
	 * Obtiene el rectangulo
	 * 
	 * @return: el rectangulo
	 */
	public Rectangle getRect() {
		return rect;
	}

	/**
	 * Fija el rectangulo
	 * 
	 * @param rect:
	 *            nuevo rectangulo
	 */
	public void setRect(Rectangle rect) {
		this.rect = rect;
	}

	/**
	 * Obtiene enimgo
	 * 
	 * @return: el enemigo
	 */
	public Sprite getEnemigoSprite() {
		return enemigoSprite;
	}

	/**
	 * Fija nuevo enemigo
	 * 
	 * @param enemigoSprite:
	 *            nuevo enemigo
	 */
	public void setEnemigoSprite(Sprite enemigoSprite) {
		this.enemigoSprite = enemigoSprite;
	}

	/**
	 * Obtiene la vida
	 * 
	 * @return: vida
	 */
	public int getVida() {
		return vida;
	}

	/**
	 * Fija la vida
	 * 
	 * @param vida:
	 *            nueva vida
	 */
	public void setVida(int vida) {
		this.vida = vida;
	}
}
