package state;

import java.awt.Canvas;
import java.awt.Graphics2D;

/**
 * 
 * @author mende_000
 *
 */
public abstract class Interface {
	private MaquinaEstados maquinaEstados;

	/**
	 * Constructor
	 * 
	 * @param maquinaEstados
	 */
	public Interface(MaquinaEstados maquinaEstados) {
		this.maquinaEstados = maquinaEstados;
	}

	/**
	 * Actualiza
	 * 
	 * @param delta
	 */
	public abstract void update(double delta);

	/**
	 * Dibuja
	 * 
	 * @param g
	 */
	public abstract void draw(Graphics2D g);

	/**
	 * Inicia en canvas
	 * 
	 * @param canvas
	 */
	public abstract void init(Canvas canvas);

	/**
	 * Obtiene la maquina de estados
	 * 
	 * @return: la maquina de estados
	 */
	public MaquinaEstados getMaquinaEstados() {
		return maquinaEstados;
	}

}
