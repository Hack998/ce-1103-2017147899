package state;

import java.awt.Canvas;
import java.awt.Graphics2D;
import java.util.ArrayList;

import menu.Menu;
import pantalla.Pantalla;

/**
 * 
 * @author mende_000
 *
 */
public class MaquinaEstados {

	private ArrayList<Interface> states = new ArrayList<Interface>();
	private Canvas canvas;
	private byte selectState = 0;

	/**
	 * Constructor
	 * 
	 * @param canvas
	 */
	public MaquinaEstados(Canvas canvas) {
		Interface game = new Pantalla(this);
		Interface menu = new Menu(this);
		states.add(menu);
		states.add(game);
		this.canvas = canvas;
	}

	/**
	 * Dibuja
	 * 
	 * @param g
	 */
	public void draw(Graphics2D g) {
		states.get(selectState).draw(g);
	}

	/**
	 * Actualiza
	 * 
	 * @param delta
	 */
	public void update(double delta) {
		states.get(selectState).update(delta);
	}

	/**
	 * Fija el Estado
	 * 
	 * @param i
	 */
	public void setState(byte i) {
		for (int r = 0; r < canvas.getKeyListeners().length; r++)
			canvas.removeKeyListener(canvas.getKeyListeners()[r]);
		selectState = i;
		states.get(selectState).init(canvas);
	}

	/**
	 * Obtiene el estado
	 * 
	 * @return: el estado
	 */
	public byte getState() {
		return selectState;
	}
}
