package display;

import java.awt.BorderLayout;
import javax.swing.JApplet;

/**
 * 
 * @author mende_000
 *
 */
public class Display_Applet extends JApplet {

	private static final long serialVersionUID = 1L;
	private Display display = new Display();

	/**
	 * Crea la pnatalla
	 */
	public void init() {
		setLayout(new BorderLayout());
		add(display);
	}

	/**
	 * Inicia la pantalla
	 */
	public void start() {
		display.start();
	}

	/**
	 * Detiene la Pantalla
	 */
	public void stop() {
		display.stop();
	}
}
