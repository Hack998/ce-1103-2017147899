package balas;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import display.Display;

/**
 * 
 * @author mende_000
 *
 */
public class Pistola extends Weapon {

	private Rectangle bullet;
	private final double speed = 4.5d;

	/**
	 * Constructor
	 * 
	 * @param xPos:
	 *            posicion en x
	 * @param yPos:
	 *            posicion en y
	 * @param width:
	 *            ancho
	 * @param height:
	 *            alto
	 */
	public Pistola(double xPos, double yPos, int width, int height) {
		this.setxPos(xPos);
		this.setyPos(yPos);
		this.setWidth(width);
		this.setHeight(height);
		this.bullet = new Rectangle((int) getxPos(), (int) getyPos(), getWidth(), getHeight());
	}

	/**
	 * Dibuja
	 */
	@Override
	public void draw(Graphics2D g) {
		if (bullet == null)
			return;
		g.setColor(Color.BLUE);
		g.fill(bullet);
	}

	/**
	 * Actualiza
	 */
	@Override
	public void update(double delta) {
		if (bullet == null)
			return;
		this.setyPos(getyPos() - (delta * speed));
		bullet.y = (int) this.getyPos();
		fueraLimite();
	}

	/**
	 * Coliciones
	 */
	@Override
	public boolean colicionRect(Rectangle rect) {
		if (this.bullet == null)
			return false;
		if (bullet.intersects(rect)) {
			this.bullet = null;
			return true;
		}
		return false;
	}

	/**
	 * Destrulle
	 */
	@Override
	public boolean destroy() {
		if (bullet == null)
			return true;
		return false;
	}

	/**
	 * Limites
	 */
	@Override
	public void fueraLimite() {
		if (this.bullet == null)
			return;
		if (bullet.y < 0 || bullet.y > Display.HEIGHT || bullet.x < 0 || bullet.x > Display.WIDTH) {
			bullet = null;
		}
	}

}
