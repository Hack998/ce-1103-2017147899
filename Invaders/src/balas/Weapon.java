package balas;

import java.awt.Graphics2D;
import java.awt.Rectangle;

/**
 * 
 * @author mende_000
 *
 */
public abstract class Weapon {
	protected double xPos;
	protected double yPos;
	protected int width;
	protected int height;

	/**
	 * Dibuja
	 * 
	 * @param g
	 */
	public abstract void draw(Graphics2D g);

	/**
	 * Actualiza
	 * 
	 * @param delta
	 */
	public abstract void update(double delta);

	/**
	 * Coliciones
	 * 
	 * @param rect:
	 *            Hilera de enemigos
	 * @return True si coliciona, False si no coliciona
	 */
	public abstract boolean colicionRect(Rectangle rect);

	/**
	 * Destruye
	 * 
	 * @return True si hay que destruir, False si no hay que destruir
	 */
	public abstract boolean destroy();

	/**
	 * Valida los limites
	 */
	public abstract void fueraLimite();

	/**
	 * Obtiene la posicion en x
	 * 
	 * @return posicion en x
	 */
	public double getxPos() {
		return xPos;
	}

	/**
	 * Fija una posicion en x
	 * 
	 * @param xPos:
	 *            nueva posicion en x
	 */
	public void setxPos(double xPos) {
		this.xPos = xPos;
	}

	/**
	 * Obtiene la posicion en y
	 * 
	 * @return la posicion en y
	 */
	public double getyPos() {
		return yPos;
	}

	/**
	 * Fija una posicion en y
	 * 
	 * @param yPos:
	 *            nueva posicion en y
	 */
	public void setyPos(double yPos) {
		this.yPos = yPos;
	}

	/**
	 * Obtiene el ancho
	 * 
	 * @return el ancho
	 */
	public int getWidth() {
		return width;
	}

	/**
	 * Fija un ancho
	 * 
	 * @param width:
	 *            nuevo ancho
	 */
	public void setWidth(int width) {
		this.width = width;
	}

	/**
	 * Obtiene el alto
	 * 
	 * @return el alto
	 */
	public int getHeight() {
		return height;
	}

	/**
	 * Fija el alto
	 * 
	 * @param height:
	 *            nuevo alto
	 */
	public void setHeight(int height) {
		this.height = height;
	}

}
