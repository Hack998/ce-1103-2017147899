package hilera;

import java.awt.Graphics2D;

/**
 * 
 * @author mende_000
 *
 */
public class Circular extends Hilera {
	private Nodo inicio;
	private int tama�o;
	private Nodo ultimo;

	/**
	 * Constructor
	 */
	public Circular() {
		this.inicio = null;
		this.ultimo = null;
		this.tama�o = 0;
	}

	/**
	 * Revisa si la lista esta vacia
	 */
	@Override
	public boolean esVacia() {
		return inicio == null;
	}

	/**
	 * Obtiene el tama�o de la lista
	 */
	@Override
	public int getTama�o() {
		return tama�o;
	}

	/**
	 * Agrega un objeto al inicia de la lista
	 */
	@Override
	public void agregarAlInicio(Object valor) {
		Nodo nuevo = new Nodo();
		nuevo.setValor(valor);
		if (esVacia()) {
			inicio = nuevo;
			ultimo = nuevo;
			ultimo.setSiguiente(inicio);
		} else {
			nuevo.setSiguiente(inicio);
			inicio = nuevo;
			ultimo.setSiguiente(inicio);
		}
		tama�o++;
	}

	/**
	 * Remueve un nodo de la lista segun su posicion
	 */
	@Override
	public void removerPorPosicion(int posicion) {
		if (posicion >= 0 && posicion < tama�o) {
			if (posicion == 0) {
				inicio = inicio.getSiguiente();
				ultimo.setSiguiente(inicio);
			} else {
				Nodo aux = inicio;
				for (int i = 0; i < posicion - 1; i++) {
					aux = aux.getSiguiente();
				}
				if (aux.getSiguiente() == ultimo) {
					aux.setSiguiente(inicio);
					ultimo = aux;
				} else {
					Nodo siguiente = aux.getSiguiente();
					aux.setSiguiente(siguiente.getSiguiente());
				}
			}
			tama�o--;
			if (tama�o == 0)
				inicio = null;
		}
	}

	/**
	 * Elimina la lista
	 */
	@Override
	public void eliminar() {
		inicio = null;
		ultimo = null;
		tama�o = 0;
	}

	/**
	 * Obtiene el valor de un nodo segun su posicion
	 */
	@Override
	public Object getValor(int posicion) {
		if (posicion >= 0 && posicion < tama�o) {
			if (posicion == 0) {
				return inicio.getValor();
			} else {
				Nodo aux = inicio;
				for (int i = 0; i < posicion; i++) {
					aux = aux.getSiguiente();
				}
				return aux.getValor();
			}
		}
		return posicion;
	}

	/**
	 * Dibuja
	 */
	@Override
	public void draw(Graphics2D g) {

	}

	/**
	 * Cambia nodos
	 */
	@Override
	public void cambiar(int posicionJefe, int posicion) {

	}

}
