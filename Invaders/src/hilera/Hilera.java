package hilera;

import java.awt.Graphics2D;

/**
 * 
 * @author mende_000
 *
 */
public abstract class Hilera {
	/**
	 * Revisa si la lista esta vacia
	 * 
	 * @return: True si esta vacia, False si no esta vacia
	 */
	public abstract boolean esVacia();

	/**
	 * Obtiene el tama�o de la lista
	 * 
	 * @return: el tama�o de la lista
	 */
	public abstract int getTama�o();

	/**
	 * Inserta un nodo al inicio de la lista
	 * 
	 * @param valor:
	 *            el valor del nodo a insertar
	 */
	public abstract void agregarAlInicio(Object valor);

	/**
	 * Remueve un nodo de lalista segun su posicion
	 * 
	 * @param posicion:
	 *            posicion del nodo a eliminar
	 */
	public abstract void removerPorPosicion(int posicion);

	/**
	 * Elimina la lista
	 */
	public abstract void eliminar();

	/**
	 * Obtiene el valor de un nodo segun su posicion
	 * 
	 * @param posicion:
	 *            posicion del nodo que se necesita el valor
	 * @return: el valor del nodo
	 */
	public abstract Object getValor(int posicion);

	/**
	 * Dibuja
	 * 
	 * @param g
	 */
	public abstract void draw(Graphics2D g);

	/**
	 * Cambia nodos
	 * 
	 * @param posicionJefe:
	 *            nodo a cambiar
	 * @param posicion:
	 *            nodo con el cual cambia
	 */
	public abstract void cambiar(int posicionJefe, int posicion);
}
