package hilera;

/**
 * 
 * @author mende_000
 *
 */
public class Nodo {
	private Object valor;
	private Nodo siguiente;
	private Nodo anterior;

	/**
	 * Constrctor
	 */
	public Nodo() {
		this.valor = null;
		this.siguiente = null;
		this.anterior = null;
	}

	/**
	 * Obtiene el valor del nodo
	 * 
	 * @return: el valor del nodo
	 */
	public Object getValor() {
		return valor;
	}

	/**
	 * Fija un valor al nodo
	 * 
	 * @param valor:
	 *            el nuevo valor del nodo
	 */
	public void setValor(Object valor) {
		this.valor = valor;
	}

	/**
	 * Obtiene el nodo siguiente
	 * 
	 * @return: el nodo siguiente
	 */
	public Nodo getSiguiente() {
		return siguiente;
	}

	/**
	 * Fija el nodo siguente
	 * 
	 * @param siguiente:
	 *            nuevo nodo siguiente
	 */
	public void setSiguiente(Nodo siguiente) {
		this.siguiente = siguiente;
	}

	/**
	 * Obtiene el nodo anterior
	 * 
	 * @return: el nodo anterior
	 */
	public Nodo getAnterior() {
		return anterior;
	}

	/**
	 * Fija el nodo anterior
	 * 
	 * @param anterior:
	 *            nuevo nodo anterior
	 */
	public void setAnterior(Nodo anterior) {
		this.anterior = anterior;
	}
}
