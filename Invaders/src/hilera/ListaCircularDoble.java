package hilera;

import java.awt.Graphics2D;

/**
 * 
 * @author mende_000
 *
 */
public class ListaCircularDoble extends Circular {
	private Nodo inicio;
	private int tama�o;
	private Nodo ultimo;

	/**
	 * Constructor
	 */
	public ListaCircularDoble() {
		this.inicio = null;
		this.ultimo = null;
		this.tama�o = 0;
	}

	/**
	 * Revisa si la lista esta vacia
	 */
	@Override
	public boolean esVacia() {
		return inicio == null;
	}

	/**
	 * Obtiene el tama�o de la lista
	 */
	@Override
	public int getTama�o() {
		return tama�o;
	}

	/**
	 * Agrega un nodo al inicio de la lista
	 */
	@Override
	public void agregarAlInicio(Object valor) {
		Nodo nuevo = new Nodo();
		nuevo.setValor(valor);
		if (esVacia()) {
			inicio = nuevo;
			ultimo = nuevo;
			ultimo.setSiguiente(inicio);
		} else {
			nuevo.setSiguiente(inicio);
			inicio = nuevo;
			inicio.getSiguiente().setAnterior(inicio);
			inicio.setAnterior(ultimo);
			ultimo.setSiguiente(inicio);
		}
		tama�o++;
	}

	/**
	 * Remueve un nodo segun su posicion
	 */
	public void removerPorPosicion(int posicion) {
		if (posicion >= 0 && posicion < tama�o) {
			if (posicion == 0) {
				inicio = inicio.getSiguiente();
				inicio.setAnterior(ultimo);
				ultimo.setSiguiente(inicio);
			} else {
				Nodo aux = inicio;
				for (int i = 0; i < posicion - 1; i++) {
					aux = aux.getSiguiente();
				}
				if (aux.getSiguiente() == ultimo) {
					aux.setSiguiente(inicio);
					ultimo = aux;
					inicio.setAnterior(ultimo);
				} else {
					Nodo siguiente = aux.getSiguiente();
					aux.setSiguiente(siguiente.getSiguiente());
					siguiente.getSiguiente().setAnterior(aux);
				}
			}
			tama�o--;
			if (tama�o == 0)
				inicio = null;
		}
	}

	/**
	 * Obtiene el valor de un nodo segun su posicion
	 */
	@Override
	public Object getValor(int posicion) {
		if (posicion >= 0 && posicion < tama�o) {
			if (posicion == 0) {
				return inicio.getValor();
			} else {
				Nodo aux = inicio;
				for (int i = 0; i < posicion; i++) {
					aux = aux.getSiguiente();
				}
				return aux.getValor();
			}
		}
		return posicion;
	}

	/**
	 * Dibuja
	 */
	@Override
	public void draw(Graphics2D g) {

	}
}
