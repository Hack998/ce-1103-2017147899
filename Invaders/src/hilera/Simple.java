package hilera;

import java.awt.Graphics2D;

/**
 * 
 * @author mende_000
 *
 */
public class Simple extends Hilera {
	private Nodo inicio;
	private int tama�o;

	/**
	 * Constructor
	 */
	public Simple() {
		this.inicio = null;
		this.tama�o = 0;
	}

	/**
	 * Verifica si la lista esta vacia
	 */
	@Override
	public boolean esVacia() {
		return inicio == null;
	}

	/**
	 * Obtiene el tama�o de la lista
	 */
	@Override
	public int getTama�o() {
		return tama�o;
	}

	/**
	 * Agrega un nodo al inicio de la lista
	 */
	@Override
	public void agregarAlInicio(Object valor) {
		Nodo nuevo = new Nodo();
		nuevo.setValor(valor);
		if (esVacia()) {
			inicio = nuevo;
		} else {
			nuevo.setSiguiente(inicio);
			inicio = nuevo;
		}
		tama�o += 1;
	}

	/**
	 * Remueve un nodo segun su posicion
	 */
	@Override
	public void removerPorPosicion(int posicion) {
		if (posicion >= 0 && posicion < tama�o) {
			if (posicion == 0) {
				inicio = inicio.getSiguiente();
			} else {
				Nodo aux = inicio;
				for (int i = 0; i < posicion - 1; i++) {
					aux = aux.getSiguiente();
				}
				Nodo siguiente = aux.getSiguiente();
				aux.setSiguiente(siguiente.getSiguiente());
			}
			tama�o--;
		}
	}

	/**
	 * elimina la lista
	 */
	@Override
	public void eliminar() {
		inicio = null;
		tama�o = 0;
	}

	/**
	 * Obtiene el valor de un nodo segun su posicion
	 */
	@Override
	public Object getValor(int posicion) {
		if (posicion >= 0 && posicion < tama�o) {
			if (posicion == 0) {
				return inicio.getValor();
			} else {
				Nodo aux = inicio;
				for (int i = 0; i < posicion; i++) {
					aux = aux.getSiguiente();
				}
				return aux.getValor();

			}
		}
		return posicion;
	}

	/**
	 * Dibuja
	 */
	@Override
	public void draw(Graphics2D g) {

	}

	/**
	 * Cambia nodos
	 */
	@Override
	public void cambiar(int posicionJefe, int posicion) {

	}
}
