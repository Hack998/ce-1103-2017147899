package sprite;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;

import javax.imageio.ImageIO;

/**
 * 
 * @author mende_000
 *
 */
public class Sprite {

	private BufferedImage sprites;
	private byte currentSprite;
	private boolean destruir = false;
	private boolean play = false;
	private double xPos;
	private double yPos;
	private int width;
	private int height;

	/**
	 * Constructor
	 * 
	 * @param xPos:
	 *            posicion en x
	 * @param yPos:
	 *            posicion en y
	 * @param width:
	 *            ancho
	 * @param height:
	 *            alto
	 * @param imgPath:
	 *            imagen
	 */
	public Sprite(double xPos, double yPos, int width, int height, String imgPath) {
		this.xPos = xPos;
		this.yPos = yPos;
		try {
			URL url = this.getClass().getResource(imgPath);
			BufferedImage pSprite = ImageIO.read(url);
			for (int y = 0; y < width; y++) {
				for (int x = 0; x < height; x++) {
					addSprite(pSprite);
				}
			}
		} catch (IOException e) {
		}
		;
	}

	/**
	 * Dibuja
	 * 
	 * @param g
	 */
	public void draw(Graphics2D g) {
		if (AnimationDestroy())
			return;
		g.drawImage(sprites, (int) getxPos(), (int) getyPos(), width, height, null);
	}

	/**
	 * Actualiza
	 * 
	 * @param delta
	 */
	public void update(double delta) {
		if (AnimationDestroy())
			return;
	}

	/**
	 * Animacion de destruccion
	 * 
	 * @return
	 */
	public boolean AnimationDestroy() {
		if (sprites == null)
			return true;
		return false;
	}

	/**
	 * A�ade sprite
	 * 
	 * @param spriteMap
	 */
	public void addSprite(BufferedImage spriteMap) {
		sprites = spriteMap;
	}

	/**
	 * Revisa el jugador
	 * 
	 * @param play:
	 *            si esta jugando
	 * @param destruir:
	 *            si ha sido destruido
	 */
	public void Player(boolean play, boolean destruir) {
		this.play = play;
		this.setDestruir(destruir);
	}

	/**
	 * Obtiene el sprite actual
	 * 
	 * @return: Sprite actial
	 */
	public byte getCurrentSprite() {
		return currentSprite;
	}

	/**
	 * Fija el sprite actual
	 * 
	 * @param currentSprite
	 */
	public void setCurrentSprite(byte currentSprite) {
		this.currentSprite = currentSprite;
	}

	/**
	 * Obtine la posicion en x
	 * 
	 * @return: la posicion en x
	 */
	public double getxPos() {
		return xPos;
	}

	/**
	 * Fija la posicion en x
	 * 
	 * @param xPos:
	 *            nueva posicion en x
	 */
	public void setxPos(double xPos) {
		this.xPos = xPos;
	}

	/**
	 * Obtiene la posicion en y
	 * 
	 * @return: la posicion en y
	 */
	public double getyPos() {
		return yPos;
	}

	/**
	 * Fija la posicion en y
	 * 
	 * @param yPos:
	 *            nueva posicion en y
	 */
	public void setyPos(double yPos) {
		this.yPos = yPos;
	}

	/**
	 * Obtiene destruir
	 * 
	 * @return: true si hay que destruir, false si no hay que destruir
	 */
	public boolean getDestruir() {
		return destruir;
	}

	/**
	 * Fija destruir
	 * 
	 * @param destruir:
	 *            nuevo destruir
	 */
	public void setDestruir(boolean destruir) {
		this.destruir = destruir;
	}

	/**
	 * Valida si esta jugando
	 * 
	 * @return: true si esta jugando, false si no esta jugando
	 */
	public boolean isPlay() {
		return play;
	}

	/**
	 * Obtiene el ancho
	 * 
	 * @return: el ancho
	 */
	public int getWidth() {
		return width;
	}

	/**
	 * Fija el ancho
	 * 
	 * @param width:
	 *            nuevo ancho
	 */
	public void setWidth(int width) {
		this.width = width;
	}

	/**
	 * obtiene el alto
	 * 
	 * @return: el alto
	 */
	public int getHeight() {
		return height;
	}

	/**
	 * Fija el alto
	 * 
	 * @param height:
	 *            nuevo alto
	 */
	public void setHeight(int height) {
		this.height = height;
	}
}
