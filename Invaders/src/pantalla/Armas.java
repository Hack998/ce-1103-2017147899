package pantalla;

import java.awt.Graphics2D;
import java.util.ArrayList;

import balas.Pistola;
import balas.Weapon;
import timer.Timer;

/**
 * 
 * @author mende_000
 *
 */
public class Armas {

	private Timer timer;
	public ArrayList<Weapon> weapons = new ArrayList<Weapon>();

	/**
	 * Constructor
	 */
	public Armas() {
		timer = new Timer();
	}

	/**
	 * Dibuja
	 * 
	 * @param g
	 */
	public void draw(Graphics2D g) {
		for (int i = 0; i < weapons.size(); i++) {
			weapons.get(i).draw(g);
		}
	}

	/**
	 * Actualiza
	 * 
	 * @param delta
	 */
	public void update(double delta) {
		for (int i = 0; i < weapons.size(); i++) {
			weapons.get(i).update(delta);
			if (weapons.get(i).destroy())
				weapons.remove(i);
		}
	}

	/**
	 * Dispara la bala
	 * 
	 * @param xPos:
	 *            posicion en x
	 * @param yPos:
	 *            posicion en y
	 * @param width:
	 *            ancho de la bala
	 * @param height:
	 *            alto de la bala
	 */
	public void shootBullet(double xPos, double yPos, int width, int height) {
		if (timer.timerEvent(500))
			weapons.add(new Pistola(xPos + 22, yPos + 15, width, height));
	}
}
