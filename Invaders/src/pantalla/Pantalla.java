package pantalla;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;

import display.Display;
import niveles.Level1;
import puntuacion.Guardar;
import puntuacion.Leer;
import state.Interface;
import state.MaquinaEstados;
import timer.TickTimer;

public class Pantalla extends Interface {

	private Jugador jugador;
	private Level1 level;
	private static int score = 0;
	private static int maxScore = 0;
	private static int hilera = (int) (Math.random() * 6) + 1;
	private static int proxHilera = (int) (Math.random() * 6) + 1;
	private static int muertes = 0;
	private static int contador = 0;
	private TickTimer gameoverTimer = new TickTimer(100);
	private Font gameScreem = new Font("Arial", Font.PLAIN, 48);
	private static int nivel = 1;

	/**
	 * Constructor
	 * 
	 * @param maquinaEstados:
	 *            maquina de estados
	 */
	public Pantalla(MaquinaEstados maquinaEstados) {
		super(maquinaEstados);
		jugador = new Jugador(Display.WIDTH / 2 - 50, Display.HEIGHT - 75, 40, 40);
		level = new Level1(jugador);
	}

	/**
	 * Actualiza
	 */
	@Override
	public void update(double delta) {
		jugador.update(delta);
		level.update(delta);
		if (contador == 5) {
			hilera = proxHilera;
			proxHilera = (int) (Math.random() * 6) + 1;
			contador = 0;
		}
		if (muertes == 18) {
			++nivel;
			muertes = 0;
		}
		if (level.gameover()) {
			gameoverTimer.tick(delta);
			if (gameoverTimer.isReady()) {
				level.reset();
				getMaquinaEstados().setState((byte) 0);
				score = 0;
			}
		}
	}

	/**
	 * Dibuja
	 */
	@Override
	public void draw(Graphics2D g) {
		g.setColor(Color.RED);
		g.drawString("Puntuacion: " + score, 5, 15);
		g.setColor(Color.RED);
		g.drawString("Puntuacion Maxima: " + maxScore, 5, 30);
		g.setColor(Color.RED);
		g.drawString("Hilera Actual: " + hilera(hilera), 650, 15);
		g.setColor(Color.RED);
		g.drawString("Proxima Hilera: " + hilera(proxHilera), 650, 30);
		g.setColor(Color.RED);
		g.drawString("Nivel: " + nivel, 385, 22);
		jugador.draw(g);
		level.draw(g);
		Leer.leer();
		if (level.gameover()) {
			g.setColor(Color.RED);
			g.setFont(gameScreem);
			String gameOver = "GAME OVER!";
			int save = 0;
			if (score > maxScore && save == 0) {
				Guardar.guardar(score);
				save = 1;
			}
			g.drawString(gameOver, (Display.WIDTH / 2) - (g.getFontMetrics().stringWidth(gameOver) / 2),
					Display.HEIGHT / 2);
		}
	}

	/**
	 * Valida el tipo de hilera segun un numero
	 * 
	 * @param hilera:
	 *            el numero de tipo de hilera
	 * @return: string con el tipo de hilera
	 */
	public String hilera(int hilera) {
		if (hilera == 1)
			return "Basica";
		else if (hilera == 2)
			return "Tipo A";
		else if (hilera == 3)
			return "Tipo B";
		else if (hilera == 4)
			return "Tipo C";
		else if (hilera == 5)
			return "Tipo D";
		else if (hilera == 6)
			return "Tipo E";
		return null;
	}

	/**
	 * Agrega las teclas
	 */
	@Override
	public void init(Canvas canvas) {
		canvas.addKeyListener(jugador);
	}

	/**
	 * Obtiene el puntaje
	 * 
	 * @return: el puntaje
	 */
	public static int getScore() {
		return score;
	}

	/**
	 * Fija un puntaje
	 * 
	 * @param score:
	 *            nuevo puntaje
	 */
	public static void setScore(int score) {
		Pantalla.score = score;
	}

	/**
	 * Fija las muertes de los enemigos
	 * 
	 * @param muertes:
	 *            nueva coantidad de enemigos eliminados
	 */
	public static void setMuertes(int muertes) {
		Pantalla.muertes = muertes;
	}

	/**
	 * obtiene la cantidad de enemigos eliminados
	 * 
	 * @return: la cantidad de enemigos eliminados
	 */
	public static int getMuertes() {
		return muertes;
	}

	/**
	 * Fija un contador
	 * 
	 * @param contador:
	 *            contador nuevo
	 */
	public static void setContador(int contador) {
		Pantalla.contador = contador;
	}

	/**
	 * Fija la puntuacion maxima
	 * 
	 * @param maxScore:
	 *            nueva puntuacion maxima
	 */
	public static void setMaxScore(int maxScore) {
		Pantalla.maxScore = maxScore;
	}

	/**
	 * Obtiene la hilera de enemigos
	 * 
	 * @return: la hilera de enemigos
	 */
	public static int getHilera() {
		return hilera;
	}

	/**
	 * Obtiene el nivel
	 * 
	 * @return: el nivel
	 */
	public static int getNivel() {
		return nivel;
	}

	/**
	 * Obtiene el contador
	 * 
	 * @return: el contador
	 */
	public static int getContador() {
		return contador;
	}

}
