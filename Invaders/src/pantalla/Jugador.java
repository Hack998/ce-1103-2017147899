package pantalla;

import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;

import javax.imageio.ImageIO;

import display.Display;

/**
 * 
 * @author mende_000
 *
 */
public class Jugador implements KeyListener {

	private final double speed = 5.0d;
	private BufferedImage pSprite;
	private Rectangle rect;
	private double xPos;
	private double yPos;
	private int width;
	private int height;
	private boolean left = false;
	private boolean right = false;
	private boolean shoot = false;
	public Armas weapon;

	/**
	 * Constructor
	 * 
	 * @param xPos:
	 *            posicion en x
	 * @param yPos:
	 *            posicion en y
	 * @param width:
	 *            ancho de la nave
	 * @param height:
	 *            alto de la nave
	 */
	public Jugador(double xPos, double yPos, int width, int height) {
		this.xPos = xPos;
		this.yPos = yPos;
		this.width = width;
		this.height = height;
		rect = new Rectangle((int) xPos, (int) yPos, width, height);
		try {
			URL url = this.getClass().getResource("/imagenes/nave.png");
			pSprite = ImageIO.read(url);
		} catch (IOException e) {
		}
		;
		weapon = new Armas();
	}

	/**
	 * Dibuja
	 * 
	 * @param g
	 */
	public void draw(Graphics2D g) {
		g.drawImage(pSprite, (int) xPos, (int) yPos, width, height, null);
		weapon.draw(g);
	}

	/**
	 * Actualiza
	 * 
	 * @param delta
	 */
	public void update(double delta) {
		if (right && !left && xPos < Display.WIDTH - width) {
			xPos += speed * delta;
			rect.x = (int) xPos;
		}
		if (!right && left && xPos > 10) {
			xPos -= speed * delta;
			rect.x = (int) xPos;
		}
		weapon.update(delta);
		if (shoot) {
			weapon.shootBullet(xPos, yPos, 5, 5);
		}
	}

	/**
	 * Valida que va a pasar al presionar una tecla
	 */
	@Override
	public void keyPressed(KeyEvent e) {
		int key = e.getKeyCode();
		if (key == KeyEvent.VK_D || key == KeyEvent.VK_RIGHT) {
			right = true;
		} else if (key == KeyEvent.VK_A || key == KeyEvent.VK_LEFT) {
			left = true;
		}

		if (key == KeyEvent.VK_SPACE) {
			shoot = true;
		}
	}

	/**
	 * Valida que va a pasar al mantener presionada una letra
	 */
	@Override
	public void keyReleased(KeyEvent e) {
		int key = e.getKeyCode();
		if (key == KeyEvent.VK_D || key == KeyEvent.VK_RIGHT) {
			right = false;
		} else if (key == KeyEvent.VK_A || key == KeyEvent.VK_LEFT) {
			left = false;
		}

		if (key == KeyEvent.VK_SPACE) {
			shoot = false;
		}
	}

	/**
	 * Valida el tipo de tecla
	 */
	@Override
	public void keyTyped(KeyEvent e) {

	}

	/**
	 * Reinica
	 */
	public void reset() {

	}

}
