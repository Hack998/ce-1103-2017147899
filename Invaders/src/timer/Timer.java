package timer;

/**
 * 
 * @author mende_000
 *
 */
public class Timer {

	private long prevTime;

	/**
	 * Constructor
	 */
	public Timer() {
		setprevTime(System.currentTimeMillis());
	}

	/**
	 * Obtiene contador
	 * 
	 * @return: contador
	 */
	public long getprevTime() {
		return prevTime;
	}

	/**
	 * Fija contador
	 * 
	 * @param currentTime:
	 *            nuevo contador
	 */
	public void setprevTime(long currentTime) {
		this.prevTime = currentTime;
	}

	/**
	 * Reinicia contador
	 */
	public void resetTimer() {
		prevTime = System.currentTimeMillis();
	}

	/**
	 * Modifica contador
	 * 
	 * @param timer:
	 *            contador
	 * @return: true si se modifico contador, false si no se modifica contador
	 */
	public boolean timerEvent(int timer) {
		if (System.currentTimeMillis() - getprevTime() > timer) {
			resetTimer();
			return true;
		}
		return false;
	}

	/**
	 * Verifica si contador esta listo
	 * 
	 * @param timer:
	 *            contador
	 * @return: true si esta listo, false si no esta listo
	 */
	public boolean ready(int timer) {
		if (System.currentTimeMillis() - getprevTime() > timer)
			return true;
		return false;
	}
}
