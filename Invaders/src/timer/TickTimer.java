package timer;

/**
 * 
 * @author mende_000
 *
 */
public class TickTimer {

	private float tick;
	private float tickTarget;

	/**
	 * Constructor
	 * 
	 * @param tickTarget
	 */
	public TickTimer(float tickTarget) {
		this.tickTarget = tickTarget;
		this.tick = 0;
	}

	/**
	 * Aumenta el contador
	 * 
	 * @param delta
	 */
	public void tick(double delta) {
		if (tick <= tickTarget) {
			tick += 1 * delta;
		}
	}

	/**
	 * Revisa si el contador esta listo
	 * 
	 * @return:True si esta listo, False si no esta listo
	 */
	public boolean isReady() {
		if (tick >= tickTarget) {
			resetTimer();
			return true;
		}
		return false;
	}

	/**
	 * Reinicia
	 */
	private void resetTimer() {
		tick = 0;
	}
}
