package puntuacion;

import java.io.File;
import java.io.FileWriter;

/**
 * 
 * @author mende_000
 *
 */
public class Guardar {
	/**
	 * Guarda el puntaje en un archivo .txt
	 * 
	 * @param puntuacion:
	 *            la puntuacion a guardar
	 */
	public static void guardar(int puntuacion) {
		try {
			String maxima = String.valueOf(puntuacion);
			File archivodel = new File("src/puntuacion.txt");
			archivodel.delete();
			File archivo = new File("src/puntuacion.txt");
			FileWriter escribir = new FileWriter(archivo, true);
			escribir.write(maxima);
			escribir.close();
		} catch (Exception e) {
		}
	}
}
