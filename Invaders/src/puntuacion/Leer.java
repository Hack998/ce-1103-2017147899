package puntuacion;

import java.io.BufferedReader;
import java.io.FileReader;
import pantalla.Pantalla;

/**
 * 
 * @author mende_000
 *
 */
public class Leer {
	/**
	 * Lee el archivo .txt y obtiene la puntuacion maxima
	 */
	public static void leer() {
		try {
			FileReader lector = new FileReader("src/puntuacion.txt");
			BufferedReader contenido = new BufferedReader(lector);
			String puntuacion = contenido.readLine();
			Pantalla.setMaxScore(Integer.valueOf(puntuacion));
			contenido.close();
		} catch (Exception e) {
		}
	}
}
