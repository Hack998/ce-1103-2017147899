package menu;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import display.Display;
import state.Interface;
import state.MaquinaEstados;

/**
 * 
 * @author mende_000
 *
 */
public class Menu extends Interface implements KeyListener {

	private Font tittleFont = new Font("Arial", Font.PLAIN, 64);
	private Font startFont = new Font("Arial", Font.PLAIN, 32);
	private String tittle = "Invaders";
	private String start = "Presione Enter";

	/**
	 * Constructor
	 * 
	 * @param maquinaEstados:
	 *            maquina de estados
	 */
	public Menu(MaquinaEstados maquinaEstados) {
		super(maquinaEstados);
	}

	/**
	 * Actualiza
	 */
	@Override
	public void update(double delta) {

	}

	/**
	 * Dibuja
	 */
	@Override
	public void draw(Graphics2D g) {
		g.setFont(tittleFont);
		int tittleWidth = g.getFontMetrics().stringWidth(tittle);
		g.setColor(Color.WHITE);
		g.drawString(tittle, ((Display.WIDTH / 2) - (tittleWidth / 2)) - 2, (Display.HEIGHT / 2) - 123);
		g.setColor(Color.BLUE);
		g.drawString(tittle, (Display.WIDTH / 2) - (tittleWidth / 2), (Display.HEIGHT / 2) - 125);
		g.setFont(startFont);
		g.setColor(Color.white);
		int startWidth = g.getFontMetrics().stringWidth(start);
		g.drawString(start, (Display.WIDTH / 2) - (startWidth / 2), (Display.HEIGHT / 2) + 75);
	}

	/**
	 * Espera a que presione una tecla
	 */
	@Override
	public void init(Canvas canvas) {
		canvas.addKeyListener(this);
	}

	/**
	 * Valida lo que va a pasar al presionar un tecla
	 */
	@Override
	public void keyPressed(KeyEvent e) {
		if (e.getKeyCode() == KeyEvent.VK_ENTER) {
			getMaquinaEstados().setState((byte) 1);
		}
	}

	/**
	 * Valida lo que pasa al mantener presionada un tecla
	 */
	@Override
	public void keyReleased(KeyEvent e) {

	}

	/**
	 * Revisa el tipo de tecla
	 */
	@Override
	public void keyTyped(KeyEvent e) {

	}
}
