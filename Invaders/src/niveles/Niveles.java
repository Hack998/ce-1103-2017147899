package niveles;

import java.awt.Graphics2D;

/**
 * 
 * @author mende_000
 *
 */
public interface Niveles {
	/**
	 * Dibuja
	 * 
	 * @param g
	 */
	void draw(Graphics2D g);

	/**
	 * Actualiza
	 * 
	 * @param delta
	 */
	void update(double delta);

	/**
	 * Valida la direccion de los enemigos
	 * 
	 * @param delta
	 */
	void direccion(double delta);

	/**
	 * Cambia la dirrecion de los enemigos
	 * 
	 * @param delta
	 */
	void cambiardireccion(double delta);

	/**
	 * Valida el Game Over
	 * 
	 * @return: True si hay Game Over, False si no hay Game Over
	 */
	boolean gameover();

	/**
	 * Destrulle
	 */
	void destruir();

	/**
	 * Reinicia
	 */
	void reset();
}
