package niveles;

import java.awt.Graphics2D;
import hilera.Hilera;
import hilera.ListaCircular;
import hilera.ListaCircularDoble;
import hilera.ListaSimple;
import hilera.ListaSimpleDoble;
import pantalla.Jugador;
import pantalla.Pantalla;
import tipoEnemigo.EnemigoBasico;
import tipoEnemigo.EnemigoJefe;
import tipoEnemigo.TipoEnemigo;

/**
 * 
 * @author mende_000
 *
 */
public class Level1 implements Niveles {

	private Jugador jugador;
	private Hilera lista;

	/**
	 * Constructor
	 * 
	 * @param jugador:
	 *            nave del jugador
	 */
	public Level1(Jugador jugador) {
		this.jugador = jugador;
		lista = addEnemigos(Pantalla.getHilera());
	}

	/**
	 * Dibuja
	 */
	@Override
	public void draw(Graphics2D g) {

		for (int i = 0; i < lista.getTama�o(); i++) {
			((TipoEnemigo) lista.getValor(i)).draw(g);
		}
	}

	/**
	 * Actualiza
	 */
	@Override
	public void update(double delta) {
		if (lista.esVacia() == true) {
			reset();
			return;
		}

		for (int i = 0; i < lista.getTama�o(); i++) {
			((TipoEnemigo) lista.getValor(i)).update(delta, jugador);
		}

		for (int i = 0; i < lista.getTama�o(); i++) {
			((TipoEnemigo) lista.getValor(i)).colision(i, jugador, lista);
		}
		direccion(delta);
	}

	/**
	 * Valida la direccion de los enemigos
	 */
	@Override
	public void direccion(double delta) {
		if (lista.esVacia() == true)
			return;

		for (int i = 0; i < lista.getTama�o(); i++) {
			if (((TipoEnemigo) lista.getValor(i)).fueraRango()) {
				cambiardireccion(delta);
			}
		}
	}

	/**
	 * Cambia la direccion de los enemigos
	 */
	@Override
	public void cambiardireccion(double delta) {
		for (int i = 0; i < lista.getTama�o(); i++) {
			((TipoEnemigo) lista.getValor(i)).cambiarDireccion(delta);

		}
	}

	/**
	 * Valida el Game Over
	 */
	@Override
	public boolean gameover() {
		for (int i = 0; i < lista.getTama�o(); i++) {
			if (((TipoEnemigo) lista.getValor(i)).fueraRango()) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Destrulle
	 */
	@Override
	public void destruir() {

	}

	/**
	 * reinicia
	 */
	@Override
	public void reset() {
		jugador.reset();
		lista.eliminar();
		lista = addEnemigos(Pantalla.getHilera());
	}

	/**
	 * Agrega los enemigos la lista
	 * 
	 * @param hilera:
	 *            lista de enemigos
	 * @return: la lista de los enemigos con ellos dentro
	 */
	public Hilera addEnemigos(int hilera) {
		if (hilera == 1) {
			ListaSimple listaB = new ListaSimple();
			for (int y = 0; y < 1; y++) {
				for (int x = 0; x < 5; x++) {
					TipoEnemigo e = new EnemigoBasico(150 + (x * 100), 50 + (y * 30), 25, 25);
					listaB.agregarAlInicio(e);
				}
			}
			return listaB;
		} else if (hilera == 2) {
			ListaSimple listaTA = new ListaSimple();
			for (int y = 0; y < 1; y++) {
				for (int x = 0; x < 5; x++) {
					if (x == 2) {
						TipoEnemigo e = new EnemigoJefe(150 + (x * 100), 50 + (y * 30), 25, 25);
						listaTA.agregarAlInicio(e);
					} else {
						TipoEnemigo e = new EnemigoBasico(150 + (x * 100), 50 + (y * 30), 25, 25);
						listaTA.agregarAlInicio(e);
					}

				}
			}
			return listaTA;
		} else if (hilera == 3) {
			ListaSimpleDoble listaTB = new ListaSimpleDoble();
			for (int y = 0; y < 1; y++) {
				for (int x = 0; x < 5; x++) {
					if (x == 2) {
						TipoEnemigo e = new EnemigoJefe(150 + (x * 100), 50 + (y * 30), 25, 25);
						listaTB.agregarAlInicio(e);
					} else {
						TipoEnemigo e = new EnemigoBasico(150 + (x * 100), 50 + (y * 30), 25, 25);
						listaTB.agregarAlInicio(e);
					}

				}
			}
			return listaTB;
		} else if (hilera == 4) {
			ListaCircular listaTC = new ListaCircular();
			for (int y = 0; y < 1; y++) {
				for (int x = 0; x < 5; x++) {
					if (x == 3) {
						TipoEnemigo e = new EnemigoJefe(150 + (x * 100), 50 + (y * 30), 25, 25);
						listaTC.agregarAlInicio(e);
					} else {
						TipoEnemigo e = new EnemigoBasico(150 + (x * 100), 50 + (y * 30), 25, 25);
						listaTC.agregarAlInicio(e);
					}

				}
			}
			return listaTC;
		} else if (hilera == 5) {
			ListaCircular listaTD = new ListaCircular();
			for (int y = 0; y < 1; y++) {
				for (int x = 0; x < 5; x++) {
					if (x == 0) {
						TipoEnemigo e = new EnemigoJefe(150 + (x * 100), 50 + (y * 30), 25, 25);
						listaTD.agregarAlInicio(e);
					} else {
						TipoEnemigo e = new EnemigoBasico(150 + (x * 100), 50 + (y * 30), 25, 25);
						listaTD.agregarAlInicio(e);
					}

				}
			}
			return listaTD;
		} else if (hilera == 6) {
			ListaCircularDoble listaTE = new ListaCircularDoble();
			for (int y = 0; y < 1; y++) {
				for (int x = 0; x < 5; x++) {
					if (x == 4) {
						TipoEnemigo e = new EnemigoJefe(150 + (x * 100), 50 + (y * 30), 25, 25);
						listaTE.agregarAlInicio(e);
					} else {
						TipoEnemigo e = new EnemigoBasico(150 + (x * 100), 50 + (y * 30), 25, 25);
						listaTE.agregarAlInicio(e);
					}

				}
			}
			return listaTE;
		}
		return null;
	}

}
